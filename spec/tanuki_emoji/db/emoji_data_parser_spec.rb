# frozen_string_literal: true

require 'rspec'

RSpec.describe TanukiEmoji::Db::EmojiDataParser do
  let(:known_properties) { TanukiEmoji::Db::EmojiDataParser::PROPERTIES.values }

  describe '#data' do
    subject(:emoji_data) { described_class.new.data }

    it 'returns an array of EmojiData' do
      is_expected.to be_a(Array)
      is_expected.to all(be_an(TanukiEmoji::Db::EmojiData))
    end

    it 'sum of parsed ranges matches metadata[:total_elements] value' do
      total_parsed_ranges = emoji_data.map { |d| d.range_size.to_i }.sum
      total_elements = described_class.new.metadata[:total_elements]

      expect(total_parsed_ranges).to eq(total_elements)
    end

    context 'for each EmojiData item' do
      it 'has a codepoint filled' do
        emoji_data.each do |data|
          expect(data.codepoints).to_not be_empty
        end
      end

      it 'has a known property filled' do
        emoji_data.each do |data|
          expect(data.property).to_not be_empty
          expect(known_properties).to include(data.property)
        end
      end

      it 'has a version filled and following a known version pattern' do
        emoji_data.each do |data|
          expect(data.version).to_not be_empty
          expect(data.version).to match(/[0-9]+\.[0-9]+/)
        end
      end

      it 'has a valid range_size' do
        emoji_data.each do |data|
          expect(data.range_size).to be >= 1
        end
      end

      it 'has examples returned as strings' do
        emoji_data.each do |data|
          expect(data.examples).to be_a(String)
        end
      end

      it 'has description filled' do
        emoji_data.each do |data|
          expect(data.description).to_not be_nil
          expect(data.description).to_not be_empty
        end
      end
    end
  end

  describe '#metadata' do
    subject(:metadata) { described_class.new.metadata }
    it 'returns a hash containing specific keys' do
      is_expected.to be_a(Hash)

      expect(metadata.keys).to include(:date, :version, :total_elements)
    end
  end

  describe '.data_file' do
    subject(:data_file) { described_class.data_file }

    it 'returns a Pathname to the default data file' do
      is_expected.to be_a(Pathname)
      expect(File.exist?(data_file)).to be_truthy
    end
  end
end
